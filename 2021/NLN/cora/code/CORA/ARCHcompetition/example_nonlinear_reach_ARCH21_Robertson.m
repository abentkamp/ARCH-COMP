function text = example_nonlinear_reach_ARCH21_Robertson()
% example_nonlinear_reach_ARCH21_Robertson - Robertson chemical reaction
%    example from the ARCH21 competition
%
% Syntax:  
%    example_nonlinear_reach_ARCH21_Robertson
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean  
% 
% Author:       Mark Wetzlinger
% Written:      17-May-2021
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

colors = {'b',[0,0.6,0],'r'};

% Parameters --------------------------------------------------------------

tFinal = 40;
params.tFinal = tFinal; 
params.U = zonotope(0);
R0 = zonotope([1;0;0]);

dim_x = dim(R0);
dim_u = dim(params.U);

% Reachability Settings ---------------------------------------------------

% Case 1: beta = 1e2, gamma = 1e3
options{1}.timeStep = 0.0025;
options{1}.taylorTerms = 6;
options{1}.zonotopeOrder = 30;
options{1}.intermediateOrder = 5;
options{1}.errorOrder = 5;

options{1}.alg = 'lin';
options{1}.tensorOrder = 3;

% Case 1: beta = 1e3, gamma = 1e5
options{2}.timeStep = 0.002;
options{2}.taylorTerms = 10;
options{2}.zonotopeOrder = 30;
options{2}.intermediateOrder = 5;
options{2}.errorOrder = 5;

options{2}.alg = 'lin';
options{2}.tensorOrder = 3;

% Case 1: beta = 1e3, gamma = 1e7
options{3}.timeStep = 0.00025;
options{3}.taylorTerms = 10;
options{3}.zonotopeOrder = 200;
options{3}.intermediateOrder = 5;
options{3}.errorOrder = 10;

options{3}.alg = 'lin';
options{3}.tensorOrder = 3;


% System Dynamics ---------------------------------------------------------

sys{1} = nonlinearSys(@Robertson_case1,dim_x,dim_u);
sys{2} = nonlinearSys(@Robertson_case2,dim_x,dim_u);
sys{3} = nonlinearSys(@Robertson_case3,dim_x,dim_u);

noSys = length(sys);


% Reachability Analysis ---------------------------------------------------

R = cell(noSys,1);
tComp = zeros(noSys,1);
widthSum = cell(3,1);

% split time horizon into two parts
tsplit = [5; 5; 2];
timeSteps = [0.0025, 0.025; 0.002, 0.005; 0.0002, 0.001];

for i=1:noSys
    
    params.R0 = R0;
    params.tFinal = tsplit(i);
    options{i}.timeStep = timeSteps(i,1);
    tic;
    R1{i} = reach(sys{i}, params, options{i});

    params.R0 = R1{i}.timePoint.set{end};
    params.tFinal = tFinal - params.tFinal;
    options{i}.timeStep = timeSteps(i,2);
    R2{i} = reach(sys{i}, params, options{i});
    % computation time
    tComp(i) = toc;
    disp(['computation time of reachable set (case ' num2str(i) ' ): ' num2str(tComp(i))]);
    
    % append R2 to R1
    R{i} = append(R1{i},R2{i});
    
    % compute sum of concentrations of all time-interval sets
	noSets = length(R{i}.timeInterval.set);

    widthSum{i} = cell(noSets,1);
    for iSet = 1:noSets
        % width (interval) of each set
        widthSum{i}{iSet,1} = sum(interval(R{i}.timeInterval.set{iSet}));
    end
    widthEnd(i,1) = 2*rad(widthSum{i}{end});
    
end



% Visualization -----------------------------------------------------------


figure; hold on; box on;

% plot sum of concentrations over time
for i=1:noSys
    Rtime = R{i}.timeInterval.time;

    for j=1:length(widthSum{i})

        % get intervals
        intX = widthSum{i}{j};
        intT = Rtime{j};

        int = cartProd(intT,intX);

        % plot interval
        han{i} = plot(int,[1,2],'FaceColor',colors{i},...
            'EdgeColor','none','Filled',true);
    end
    
end
    
% label plot
xlabel('t');
ylabel('s');
axis([0,params.tFinal,0.999,1.001]);
legend([han{1},han{2},han{3}],'Case 1', 'Case 2', 'Case 3');


% text for .csv
text{1} = ['CORA,ROBE21,gamma10e3,1,',num2str(tComp(1)),',',num2str(widthEnd(1))];
text{2} = ['CORA,ROBE21,gamma10e5,1,',num2str(tComp(2)),',',num2str(widthEnd(2))];
text{3} = ['CORA,ROBE21,gamma10e7,1,',num2str(tComp(3)),',',num2str(widthEnd(3))];


%------------- END OF CODE --------------