\section{Validation.}
\label{sec:validation}

The 2021 competition took the first steps towards validating the results produced by the tools.
The overarching goal was to ensure that the comparison reported here is meaningful,
and the approach taken accounts for several potential sources of error,
both for technical reasons or because of human error.
The hypothetical case of cheating participants was not regarded likely,
and we emphasize upfront that no indication whatsoever for dishonest behavior was found.
Rather, the goal is to establish a higher standard of quality of evaluation results,
that can ultimately benefit any future work in simulation-based falsification:
Just like the benchmark set established by this community gets adopted by experiments in the literature,
validation of results using an independent reference checker should become standard, too.

One obvious source of error are mistakes in the translation of
formal requirements into the format of the respective tool,
and similar problems with the experimental setup, like wrong input parameterization.

More subtly, there might be mismatches in the numeric computations underpinning the simulations,
due different versions of the models as well as MATLAB,
or due to different model configurations,
such as the ODE solver/integrator settings,
and the interpolation setting on the input ports.
Rounding errors introduced during pretty printing and parsing of the result format
are expected, too, and MATLAB seems to implement some effort to work around
limitations of the underlying floating point data-type,
which mismatches the way such values are handled inside the Java virtual machine on which the validator runs.

Similarly, there may be differences between computation of the robustness score,
from which falsification success is typically derived:
Four implementations are used in the competition---from \STaLiRo (also used by \ARIsTEO), \Breach (also used by \foresee),
\FalStar, and \zlscheck---whereas \FalCAuN uses boolean semantics of formulas.

To that end, for each (successful) of the 50 respective trials per requirement,
we collected the following information,
from which the above questions can be answered satisfactorily:
\begin{itemize}
\item information about which benchmark (model + requirement identifier)
\item the initial conditions and time-series input signal resulting from that trial
\item whether the signal is expected to falsify the requirement
\item if available, a robustness value derived from running the input through the model
\item optionally, the corresponding output signal, and further information such as time stamps or wall-clock times
\end{itemize}
In the following, we will refer to the this information as the ``reported'' result. Multiple questions were identified as concerns for validation, albeit not all points were achieved or fully taken into account:
\begin{enumerate}
\item does the reported input signal adhere to the valid ranges of input for that particular model, as described in \cref{sec:benchmarks}?
\item does the reported input signal adhere to the constraints of the respective instance?
\item is the reported verdict correct, i.e., whether running this signal through the model produces a falsifying trace?
\item is the reported robustness value consistent with the verdict (values strictly $<0$ indicate a falsification), and how accurately can the robustness be reproduced?
\item how accurately can the reported output be reproduced, if given?
\item are the values reported in \cref{table:allResultsInstance1,table:allResultsInstance2} correct?
\end{enumerate}
In the end, we checked for 1., 3., and 4.
Regarding 2., it is not quite straight-forward to check the shape of the signal, so we did not attempt it.
Regarding 5., not all participants did report output signals, so we omitted this aspect from further investigation.
For 6., there was simply not sufficient consistent data, as many participants chose to run a fraction only of the experiments represent in the tables.

\paragraph{Technical Details.}

A simple CSV-based format was established,%
    \footnote{\url{https://gitlab.com/gernst/ARCH-COMP/-/blob/FALS/2021/FALS/Validation.md}}
which includes one row for each result, and one column per result for the individual entries.
Compound data, specifically initial conditions and time-series signals,
are formatted as Matlab matrices that are included as quoted atomic table entries.
Reference to benchmarks was done via mnemonic keys for the models (like \texttt{AT}),
as well as for the requirements (like \texttt{AT1}),
which avoids tight coupling with a particular model file or requirement syntax.

Validation is implemented inside \FalStar.
It is based on the reference models in the folder \texttt{models/FALS} in the shared Git repository, in conjunction with a configuration file that sets up the parameter and input ranges,
and which defines the STL formulas associated with the requirements
in \FalStar's internal syntax.
The configuration file as well as the scripts used to interface with the simulation engine are
public and transparent.

The process to converge to a reasonable set of results was iterative,
where the validator, the format, and the data reported was refined multiple times,
uncovering a number of problems that were fixed one by one, with some remaining ones left open.

Once it became clear that numeric inaccuracies are prevalent,
it is likewise not reasonable to require that the robustness value as computed
during the validation run is \emph{strictly} negative,
and for the results presented here, we granted a small, benchmark-dependent tolerance,
such that positive values below this tolerance are recognized as falsifications proper,
but only if the tool reported a falsification in the first place.
The numeric tolerances have been chosen by the organizer as follows, and likely need to be refined for the next competition:
AT1: 1.2 (1\% of max speed);
AT2: 10.0 (ok wrt. the large RPM scale);
AT6*: 1.0;
AFC27: 0.0008 (10\% of $\beta$);
AFC29, AFC33: 0.0007 (10\% of $\gamma$);
NN, NNx: 0.1 (somewhat loose in comparison to the precision of the magnet)
CC1: 1.0;
SC: 0.05 (1\% of valid range).

\paragraph{Results.}

The results of the validation runs are reported in Table~\ref{table:allResultsValidation}.
Entries where the falsification rate \FR{} as reported by the participants coincides
with the number of validated experiments (column~$\checkmark$),
could be confirmed within the above stated tolerance.
Mismatches are highlighted in \textbf{bold}.
The raw data underlying this evaluation can be downloaded at \url{https://dx.doi.org/\DOI},
the validator and its configuration is available on GitLab as mentioned.

\input{results-validation}

As explained in Sec.~\ref{sec:participants}, \FalCAuN evaluates requirements over
learned discrete abstractions of the actual system. While this permits for efficient simulations,
it also introduces an error such that
a falsifying trace for the discrete system is close to a falsifying one on the original model,
but misses the mark by a small margin. On the experiments for AT6, the overall mean robustness is approximately~18,
which is double the tolerance imposed above, expressing that \FalCAuN works well in principle.
We remark that often such near-misses are of interest in practice,
since they reflect insufficient error margin in the design.

\falsify shows some discrepancy for AFC, with a mean of roughly 0.015,
significantly exceepding the accepted tolerance accepted as well as the error margin of the requirements,
for unclear reasons that we were not able to analyze in time.
We remark that the AFC model's dynamics are difficult
to integrate numerically with high accuracy (stiff differential equations),
such that even small initial perturbations or minor model variations could cause such discrepancies.

The results of \FalStar are self-validated very accurately, which is still a meaningful result,
because it confirms that no errors are introduced by serializing and re-evaluating the experimental data.
We report for most experiments, the error is exactly zero, however, a switch from generating
the data with MATLAB 2021a to validating it version 2021b introduced
some measurable inaccuracies in the order of $10^{-10}$ to $10^{-14}$,
confirming our suspicion that indeed the Matlab version might have an effect,
or perhaps inadvertently some model parameters had been changed.
For benchmark AFC33 the input signal is somehow considered invalid,
the error was found too late during preparation of this report to be investigated and corrected.

According to the participants, the unvalidated results \foresee for AT1 are
due to a reporting error, where the data indicates an
excessive number of simulations above the limit of 300 being reported,
but the actual number of simulations used could not be determined during validation
hence we chose to highlight this result.
The other discrepancies should be attributed to a mismatch between the QB-robustness
used by \foresee \cite{falsQBRobCAV2021}, which balances the magnitudes of input signals against each other.
Nevertheless, this requires further investigation as in principle the approach should be sound and therefore should not report wrong results.

\STaLiRo and \Breach both achieve very accurate validation results,
with a single defect in the experimental data of \STaLiRo due to a manual copy \& paste error
(the actual result is probably correct).
The invalid result for \Breach is due to a transcription error of the STL requirement.

\paragraph{Other Issues and Findings.}

A number of other issues was uncovered during validation:

The maximum value of the brake input of the AT model was stated correctly as 325 in previous reports,
but incorrectly as 350 in a file \texttt{requirements.txt} in the GitLab repository,
which was uncovered by checking the input ranges of reported experimental data.
This finding is in line with some feedback of participants that more documentation
on how to get started would be useful.

Another aspect was the interpretation of the robustness score for the gear value of the AT model, which is supposed to be discrete.
Hence, the robustness value of atomic propositions $g1,\ldots,g4$ of AT5* must reflect this discrete nature,
such that $\rho(gi) < 0$ if the $i$th gear is currently selected but $\rho(gi) > $ otherwise, with strict inequalities in both cases.
Indeed, some preliminary results were wrong due to an issue related to this interpretation.
Moreover, some tools return $\pm\infty$ while others $\pm1$ for AT5*, which needs to be accounted for by the validator
(\FalStar's robustness is based on the former).

There were several instances of properties being specified wrong.
For instance, \FalStar's result for AT6c from previous years employed different bounds
on the temporal operator and the input signals, which led to unsuccessful validation attempts
for the (correct) results of other tools, as the validator inherited this misconfiguration.

Finally, the time-series input signals reported by the participants came with different sampling schemes
(e.g. finely-sampled with values each <0.1 time-points, coarsely-sampled at each control point),
which requires that interpolation settings inside the validator correctly match those of the tools.
The conventions are now documented, alternatively participants resorted to reporting finely sampled inputs,
which solved numerous issues in one tool.
