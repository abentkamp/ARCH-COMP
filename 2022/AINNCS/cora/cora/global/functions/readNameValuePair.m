function [NVpairs,value] = readNameValuePair(NVpairs,name,varargin)
% readNameValuePair - searches through a list of name-value pairs to return
%    the corresponding value of a given name and a redacted list
%
% Syntax:  
%    [NVpairs,value] = readNameValuePair(NVpairs,name)
%    [NVpairs,value] = readNameValuePair(NVpairs,name,check)
%    [NVpairs,value] = readNameValuePair(NVpairs,name,check,def)
%
% Inputs:
%    NVpairs - cell-array: list of name-value pairs
%    name - char: name of name-value pair
%    check - (optional) function name or cell-array of function names:
%               check functions for value, e.g., 'isscalar'
%    def - (optional) default value
%
% Outputs:
%    NVpairs - list of name-value pairs
%    value - value of name-value pair
%
% Example: 
%    NVpairs = {'Splits',8};
%    [NVpairs,value] = readNameValuePair(NVpairs,'Splits','isscalar',10);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: global/functions/readPlotOptions, polyZonotope/plot

% Author:        Mark Wetzlinger, Niklas Kochdumper
% Written:       15-July-2020
% Last update:   24-November-2021 (allow cell-array of check functions)
% Last revision: ---

%------------- BEGIN CODE --------------

% write function into cell-array
funcs = [];
if nargin >= 3
    if ~iscell(varargin{1})
        funcs = varargin(1);
    else
        funcs = varargin{1};
    end
end

% default empty value (name not found)
value = [];
if nargin >= 4
    value = varargin{2};
end

% check every second entry
for i=1:2:length(NVpairs)-1
    
    % has to be a char (safety check) and match NVpairs
    if ischar(NVpairs{i}) && strcmp(NVpairs{i},name)
        % name found, get value
        value = NVpairs{i+1};

        % check whether name complies with check
        for j=1:length(funcs)
            if ~feval(funcs{j},value)
                error("Invalid assignment for " + name);
            end
        end

        % empty the corresponding cells
        NVpairs(i:i+1) = [];
        break
    end
end

end



%------------- END OF CODE --------------
