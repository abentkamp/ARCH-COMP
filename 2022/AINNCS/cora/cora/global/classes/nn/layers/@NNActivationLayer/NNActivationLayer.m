classdef (Abstract) NNActivationLayer < NNLayer
    % NNActivationLayer - abstract class for non-linear layers
    %
    % Syntax:
    %    obj = NNActivationLayer(name)
    %
    % Inputs:
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      28-March-2022
    % Last update:  01-April-2022 (moved to class folder)
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        is_refinable = true
    end

    properties
        % adaptive refinement
        order = 1
        refine_metric % metric for refinement
        do_refinement = 1 % e.g. if (u-l) is too large

        % function handles
        f function_handle
        df function_handle
        dfs(:, 1) cell = cell(0, 1)

        l = []
        u = []
    end

    methods
        % constructor
        function obj = NNActivationLayer(name)
            % call super class constructor
            obj@NNLayer(name)

            % init function handles
            obj.f = @(x) obj.evaluate_numeric(x);
            obj.df = obj.get_df(1);

            max_df_init = 1;
            obj.dfs = cell(0, 1);
            for i = 1:max_df_init
                obj.dfs{i, 1} = obj.get_df(i);
            end
        end

        function [nin, nout] = get_num_neurons(obj)
            nin = [];
            nout = [];
        end

        % evaluate
        r = evaluateZonotope(obj, Z, evParams)
        [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
        [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotopeLin(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
        [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotopeQuad(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
        [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotopeCub(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
        [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotopeAdaptive(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
        [c, G, Grest, d] = evaluatePolyZonotopeAdaptiveNeuron(obj, c, G, Grest, E, Es, order, ind, ind_, evParams)
        r = evaluateTaylm(obj, input, evParams)
        [c, G, C, d, l, u] = evaluateConZonotope(obj, c, G, C, d, l, u, options, evParams)

        function S = evaluate_sensitivity(obj, S, x)
            S = S * diag(obj.df(x));
        end
    end

    methods (Abstract)
        evaluate_zonotope_neuron(obj, Z)

        evaluate_polyZonotope_neuron_lin(obj, c_i, G_i, Grest_i, expMat, ...
            ind, ind_, bound_approx)
        evaluate_polyZonotope_neuron_quad(obj, c_i, G_i, Grest_i, expMat, ...
            ind, ind_, bound_approx)
        evaluate_polyZonotope_neuron_cub(obj, c_i, G_i, Grest_i, expMat, ...
            ind, ind_, bound_approx)

        get_df(obj, i)

        getDerBounds(obj, l, u)

        evaluate_taylm_neuron(obj, input, evParams)
        evaluate_conZonotope_neuron(obj, c, G, C, d, l, u, j, options, evParams)
    end

    methods (Static)
        layer = instantiateFromString(activation)
    end
end