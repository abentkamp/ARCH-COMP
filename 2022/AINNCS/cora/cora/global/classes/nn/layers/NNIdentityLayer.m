classdef NNIdentityLayer < NNLayer
    % NNIdentityLayer - class for identity layer
    % This layer is usually not necessary but can be helpful for e.g.
    % conversion from the old @neuralNetwork model or keeping order in layers
    %
    % Syntax:
    %    obj = NNLayer(name)
    %
    % Inputs:
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      28-March-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        type = "IdentityLayer"
        is_refinable = false
    end

    properties
    end

    methods
        % constructor
        function obj = NNIdentityLayer(name)
            if nargin < 1
                name = NNIdentityLayer.type;
            end
            % call super class constructor
            obj@NNLayer(name)
        end

        function [nin, nout] = get_num_neurons(obj)
            nin = [];
            nout = [];
        end

        % evaluate
        function input = evaluate_numeric(obj, input)
            % return identity
        end

        function Z = evaluateZonotope(obj, Z, evParams)
            % return identity
        end

        function [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
            % return identity
        end

        function input = evaluateTaylm(obj, input, evParams)
            % return identity
        end

        function [c, G, C, d, l, u] = evaluateConZonotope(obj, c, G, C, d, l, u, j, options, evParams)
            % return identity
        end

        function S = evaluate_sensitivity(obj, S, x)
            % return identity
        end
    end
end