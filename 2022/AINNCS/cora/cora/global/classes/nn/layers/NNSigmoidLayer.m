classdef NNSigmoidLayer < NNSShapeLayer
    % NNSigmoidLayer - class for Sigmoid layers
    %
    % Syntax:
    %    obj = NNSigmoidLayer(name)
    %
    % Inputs:
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      28-March-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        type = "SigmoidLayer"
    end

    methods
        % constructor
        function obj = NNSigmoidLayer(name)
            if nargin < 2
                name = NNSigmoidLayer.type;
            end
            % call super class constructor
            obj@NNSShapeLayer(name)
        end

        % evaluate
        function r = evaluate_numeric(obj, input)
            r = tanh(input/2) / 2 + 0.5;
        end
    end
end