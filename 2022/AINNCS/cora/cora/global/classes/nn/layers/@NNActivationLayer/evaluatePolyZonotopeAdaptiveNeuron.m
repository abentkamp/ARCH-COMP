function [c, G, Grest, d] = evaluatePolyZonotopeAdaptiveNeuron(obj, c, G, Grest, E, Es, order, ind, ind_, evParams)
% evaluates the activation neuron on a polyZonotope using polynomials of
% adaptive order
%
% Syntax:
%    [c, G, Grest, d] = evaluatePolyZonotopeAdaptiveNeuron(obj, c, G, Grest, E, Es, id, id_, ind, ind_, evParams)
%
% Inputs:
%    c, G, Grest, E, id, id_, ind, ind_ - parameters of input polyZonotope
%    Es - exponential map of output polyZonotope
%    order - polynomial degree used for approximation
%    evParams - parameter for NN evaluation
%
% Outputs:
%    updated [c, G, Grest, expMat, id, id_, ind, ind_]
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NNLayer
%
% Author:        Tobias Ladner
% Written:       28-March-2022
% Last update:   05-April-2022
%                13-May-2022 (reuse bounds)
%                24-June-2022 (performance optimizations)
% Last revision: ---

%------------- BEGIN CODE --------------

% enclose the activation function with a polynomial zonotope
% by fitting a polynomial function

f = obj.f;
df = obj.df;

% store information to directly compact generators
[G_start, G_end, G_ext_start, G_ext_end] = NNHelper.getOrderIndicesG(G, order);
[Grest_start, Grest_end, Grest_ext_start, Grest_ext_end] = NNHelper.getOrderIndicesGrest(Grest, G, order);

% whether higher orders should be enclosed
do_enclosure = false;
if ~evParams.reuse_bounds || (size(obj.l, 2) < evParams.j || size(obj.u, 2) < evParams.j)
    % compute lower and upper bound
    approx = evParams.bound_approx;
    if isa(approx, 'logical')
        [l, u] = NNHelper.compBoundsPolyZono(c, G, Grest, E, ind, ind_, approx);
    elseif strcmp(approx, "sample")
        pZ = polyZonotope(c, G, Grest, E);
        points = [pZ.randPoint(5000), pZ.randPoint(500, 'extreme')];
        tol = 0.00001;
        int = interval(min(points), max(points));
        int = int + int * tol;
        l = infimum(int);
        u = supremum(int);
    else
        error("Unknown Bound Estimation: %s", approx);
    end

    if evParams.reuse_bounds
        obj.l(evParams.j) = l;
        obj.u(evParams.j) = u;
        do_enclosure = true;
    end
else
    l = obj.l(evParams.j);
    u = obj.u(evParams.j);
end

% TODO move to NNReLULayer
if isa(obj, "NNReLULayer")
    % check if ReLU can be computed exactly
    if u <= 0
        c = 0;
        G = G * 0;
        Grest = Grest * 0;
        d = 0; % no approximation error!
        return
    elseif 0 <= l
        % identity
        % c = c;
        % G = G;
        % Grest = Grest;
        d = 0; % no approximation error!
        return
    else % l < 0 < u
        % continue approximating activation function
    end
end

approx_type = evParams.approx_type;
if strcmp(approx_type, "regression")
    if u - l > evParams.force_approx_lin_at % (u-l) too large
        % using 'lin' approximation
        lambda = min(df(l), df(u));
        mu1 = 0.5 * (f(u) + f(l) - lambda * (u + l));
        coeffs = [zeros(1, order-1), lambda, mu1];

        do_enclosure = false;
        obj.do_refinement(evParams.j) = 0;
    else
        % compute polynomial that best fits the activation function
        x = linspace(l, u, 100);
        y = f(x);
        coeffs = NNHelper.leastSquarePolyFunc(x, y, order);
        coeffs = fliplr(coeffs'); % descending order
    end
    coeffs(isnan(coeffs)) = 0;

elseif strcmp(approx_type, "throw-catch")
    if df(l) < df(u)
        coeffs = NNHelper.calcAlternatingDerCoeffs(l, u, order, f, obj.dfs);
    else
        coeffs = NNHelper.calcAlternatingDerCoeffs(u, l, order, f, obj.dfs);
    end
else
    error("Unknown approximation type '%s'", approx_type);
end

% compute the difference between activation function and quad. fit
der1 = obj.getDerBounds(l, u);
L = NNHelper.minMaxDiffOrder(order, coeffs, l, u, f, der1);

if evParams.reuse_bounds && do_enclosure
    if strcmp(approx_type, "regression")
        % also enclose all higher order polynomials
        x = linspace(l, u, 10);
        y = f(x);

        for higher_order = (order + 1):evParams.max_bounds
            % compute polynomial
            coeffs_ho = NNHelper.leastSquarePolyFunc(x, y, higher_order);
            coeffs_ho = fliplr(coeffs_ho'); % descending order
            coeffs_ho(isnan(coeffs_ho)) = 0;

            % compute the difference between activation function and p(x)
            L_ho = NNHelper.minMaxDiffOrder(higher_order, coeffs_ho, l, u, obj.f, der1);
            coeffs_ho(end) = coeffs_ho(end) + center(L_ho);
            d_ho = rad(L_ho);

            % enclose upper bound
            coeffs_ho(end) = coeffs_ho(end) + d_ho;
            L_ho = NNHelper.minMaxDiffPoly(coeffs_ho, coeffs, l, u);
            L = L | L_ho;

            % enclose lower bound
            coeffs_ho(end) = coeffs_ho(end) - 2 * d_ho;
            L_ho = NNHelper.minMaxDiffPoly(coeffs_ho, coeffs, l, u);
            L = L | L_ho;
        end
    else
        error("Reusing bounds not possible with evParams.approx_type='%s'", approx_type);
    end
end

% change polynomial s.t. lower and upper error are equal
coeffs(end) = coeffs(end) + center(L);
d = rad(L); % error is radius then.

% evaluate the polynomial approximation on the polynomial zonotope

% init
c_out = zeros(1, order);
G_out = zeros(1, G_end(end));
G_ext_out = zeros(1, G_ext_end(end));
Grest_out = zeros(1, Grest_end(end));
Grest_ext_out = zeros(1, Grest_ext_end(end));

c_out(1) = c;
G_ext_out(G_start(1):G_end(1)) = G;
Grest_ext_out(Grest_start(1):Grest_end(1)) = Grest;

for i = 2:order
    % Note that e.g., G2 * G3 = G5
    i1 = floor(i/2);
    i2 = ceil(i/2);

    % previous values
    ci1 = c_out(i1);
    Gi1 = G_ext_out(G_ext_start(i1):G_ext_end(i1));
    Gresti1 = Grest_ext_out(Grest_ext_start(i1):Grest_ext_end(i1));
    Ei1 = Es(:, G_ext_start(i1):G_ext_end(i1));

    ci2 = c_out(i2);
    Gi2 = G_ext_out(G_ext_start(i2):G_ext_end(i2));
    Gresti2 = Grest_ext_out(Grest_ext_start(i2):Grest_ext_end(i2));
    Ei2 = Es(:, G_ext_start(i2):G_ext_end(i2));

    % calculate i
    [ci, Gi_ext, Gresti_ext] = NNHelper.calcSquared(ci1, Gi1, Gresti1, Ei1, ci2, Gi2, Gresti2, Ei2, i1 == i2);

    % store results
    c_out(i) = ci;
    G_ext_out(G_ext_start(i):G_ext_end(i)) = Gi_ext;
    Grest_ext_out(Grest_ext_start(i):Grest_ext_end(i)) = Gresti_ext;
end

% update weights with coefficients
for i = 1:order
    coeff_i = coeffs(end-i);
    c_out(i) = c_out(i) * coeff_i;
    G_ext_out(G_ext_start(i):G_ext_end(i)) = G_ext_out(G_ext_start(i):G_ext_end(i)) * coeff_i;
    Grest_ext_out(Grest_ext_start(i):Grest_ext_end(i)) = Grest_ext_out(Grest_ext_start(i):Grest_ext_end(i)) * coeff_i;
end

% update generators with same exponent from back to front:
% i1 = floor(i/2); i2 = ceil(i/2)
% G{i} = [Gi1, Gi2, Gi]
% → G{i1} += Gi1
% → G{i2} += Gi2
% and add Gi to result
% analogous for Grest{i} = [Gresti1, Gresti2, Gresti]

for i = order:-1:2
    % Note that e.g., G2 * G3 = G5
    i1 = floor(i/2);
    i2 = ceil(i/2);

    % get generator lenghts
    i_len = G_end(i) - G_start(i) + 1;
    i1_len = G_ext_end(i1) - G_ext_start(i1) + 1;
    i2_len = G_ext_end(i2) - G_ext_start(i2) + 1;

    % extract lower order generators
    Gi1 = G_ext_out(1:end, G_ext_start(i)-1+(1:i1_len));
    Gi2 = G_ext_out(1:end, G_ext_start(i)+i1_len-1+(1:i2_len));
    Gi = G_ext_out(1:end, G_ext_start(i)+i1_len+i2_len-1+(1:i_len));

    % update generators
    G_ext_out(G_ext_start(i1):G_ext_end(i1)) = G_ext_out(G_ext_start(i1):G_ext_end(i1)) + Gi1;
    G_ext_out(G_ext_start(i2):G_ext_end(i2)) = G_ext_out(G_ext_start(i2):G_ext_end(i2)) + Gi2;
    G_out(G_start(i):G_end(i)) = Gi;

    % analogous for Grest if present
    if Grest_ext_end(i) > 0
        % get generator lenghts
        i_len = Grest_end(i) - Grest_start(i) + 1;
        i1_len = Grest_ext_end(i1) - Grest_ext_start(i1) + 1;
        i2_len = Grest_ext_end(i2) - Grest_ext_start(i2) + 1;

        % extract lower order generators
        Gresti1 = Grest_ext_out(1:end, Grest_ext_start(i)-1+(1:i1_len));
        Gresti2 = Grest_ext_out(1:end, Grest_ext_start(i)+i1_len-1+(1:i2_len));
        Gresti = Grest_ext_out(1:end, Grest_ext_start(i)+i1_len+i2_len-1+(1:i_len));

        % update lower order generators
        Grest_ext_out(Grest_ext_start(i1):Grest_ext_end(i1)) = Grest_ext_out(Grest_ext_start(i1):Grest_ext_end(i1)) + Gresti1;
        Grest_ext_out(Grest_ext_start(i2):Grest_ext_end(i2)) = Grest_ext_out(Grest_ext_start(i2):Grest_ext_end(i2)) + Gresti2;
        Grest_out(Grest_start(i):Grest_end(i)) = Gresti;
    end
end

% extract linear generators
G_out(G_start(1):G_end(1)) = G_ext_out(G_ext_start(1):G_ext_end(1));
Grest_out(Grest_start(1):Grest_end(1)) = Grest_ext_out(Grest_ext_start(1):Grest_ext_end(1));

c = sum(c_out) + coeffs(end);
G = G_out;
Grest = Grest_out;
end