function generateDynamicEquations(scenario, powVariables)
% generateDynamicEquations - generates the dynamic equations of a power 
% system
%
% More information can be found in [1, Sec. VII].
%
% Syntax:  
%    generateDynamicEquations(scenario, powVariables)
%
% Inputs:
%    scenario - struct specifying a power system scenario
%    powVariables - symbolic variables for the power system
%
% Outputs:
%    -
%
% References:
%    [1] M. Althoff, "Formal and Compositional Analysis of Power Systems 
%        using Reachable Sets", IEEE Transactions on Power Systems 29 (5), 
%        2014, 2270-2280
% 
% Author:       Matthias Althoff
% Written:      15-April-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% obtain bus and genParam struct
bus = scenario.bus;
genParam = scenario.genParam;

% number of buses (NoB)
NoB.generator = length(bus.generator);

if NoB.generator>0
    %remove connection to generator
    if ~isempty(bus.fault)
        genParam.X_m(bus.fault) = inf;
    end

    %obatain power system variables
    E = powVariables.E;
    V = powVariables.V;
    Theta = powVariables.Theta;
    delta = powVariables.delta;
    omega = powVariables.omega;
    T_m = powVariables.T_m;
    P_c = powVariables.P_c;


    %the first set of symbolic variables represents relative angles, the second 
    %set angular velocities, the third set torques

    %relative angles; first angle is reference and not specified; eq. (18) in [1]
    for i = 1:NoB.generator
        %init
        f(i, 1) = omega(i) - genParam.omega_s;
    end

    % angular velocities; eq. (18) in [1]
    for i = 1:NoB.generator
        %init
        f(NoB.generator + i, 1) = -genParam.D(i)/genParam.M(i)*(omega(i) - genParam.omega_s)...
            + 1/genParam.M(i)*T_m(i) - E(i)*V(i)/(genParam.M(i)*genParam.X_m(i))*sin(delta(i) - Theta(i));
    end

    % torques; eq. (18) in [1]
    for i = 1:NoB.generator
        %init
        f(2*NoB.generator + i, 1) = -1/(genParam.T_sv(i)*genParam.R_d(i)*genParam.omega_s)*(omega(i) - genParam.omega_s)...
            - 1/genParam.T_sv(i)*T_m(i) + 1/genParam.T_sv(i)*P_c(i);
    end
else
    f = [];
end

% create file
createFileFromFunction(f,[scenario.name,'_dyn'],'f','x,y,u');


%------------- END OF CODE --------------