function E = enclose(E,varargin)
% enclose - encloses an ellipsoid and its affine transformation according
%    to Prop. 2.7 in [1]
%
% Description:
%    Computes the set
%    { a x1 + (1 - a) * (M x1 + x2) | x1 \in E, x2 \in E2, a \in [0,1] }
%    where E2 = M*E + Eplus
%
% Syntax:  
%    E = enclose(E,E2)
%    E = enclose(E,M,Eplus)
%
% Inputs:
%    E - ellipsoid object
%    E2 - ellipsoid object
%    M - matrix for the linear transformation
%    Eplus - ellipsoid object added to the linear transformation
%
% Outputs:
%    E - ellipsoid that encloses E and E2
%
% Example: 
%    E1 = ellipsoid(eye(2));
%    E2 = ellipsoid([1,0;0,3],[1;-1]);
%    E = enclose(E1,E2);
%    
%    figure; hold on;
%    plot(E1);
%    plot(E2,[1,2],'r');
%    plot(E,[1,2],'g');
%
% References:
%    [1] E. Yildirim. "On the minimum volume covering ellipsoid of
%        ellipsoids", 2006
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/enclose

% Author:       Victor Gassmann
% Written:      13-March-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% parse input arguments
if nargin == 2
    E2 = varargin{1};
elseif nargin == 3
    E2 = (varargin{1}*E) + varargin{2};
else
    throw(CORAerror('CORA:tooManyInputArgs',3));
end

% compute enclosure using convex hull
E = convHull(E,E2);

%------------- END OF CODE --------------