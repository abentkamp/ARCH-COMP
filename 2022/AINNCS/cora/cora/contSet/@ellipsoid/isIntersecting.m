function res = isIntersecting(E,S,varargin)
% isIntersecting - determines if an ellipsoid intersects a set
%
% Syntax:  
%    res = isIntersecting(E,S)
%    res = isIntersecting(E,S,type)
%
% Inputs:
%    E - ellipsoid object
%    S - contSet object
%    type - type of check ('exact' or 'approx')
%
% Outputs:
%    res - true/false
%
% Example: 
%    E1 = ellipsoid([5 7;7 13],[1;2]);
%    E2 = ellipsoid(0.3*eye(2),[1;0]);
%    E3 = ellipsoid(0.3*eye(2),[2;0]);
%    Z = zonotope([3;0],0.5*eye(2));
%
%    isIntersecting(E1,E2)
%    isIntersecting(E1,E3)
%    isIntersecting(E1,Z,'approx')
%
%    figure; hold on
%    plot(E1,[1,2],'b');
%    plot(E2,[1,2],'g');
%
%    figure; hold on
%    plot(E1,[1,2],'b');
%    plot(E3,[1,2],'r');
%
%    figure; hold on
%    plot(E1,[1,2],'b');
%    plot(Z,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: conZonotope/isIntersecting

% Author:       Victor Gassmann, Niklas Kochdumper
% Written:      13-March-2019 
% Last update:  21-November-2019 (NK: extended to other sets)
%               10-March-2021 (refactored, simplified)
%               19-May-2022 (VG: removed try-catch)
% Last revision:---

%------------- BEGIN CODE --------------

if isempty(S)
    res = true;
    return;
elseif isempty(E)
    res = false;
    return;
end

% check if obj.Q is all-zero
if rank(E)==0
    res = in(S,E.q);
    return;
end

% parse input arguments
[type] = setDefaultValues({{'exact'}},varargin{:});

% check input arguments
inputArgsCheck({{E,'att',{'ellipsoid'},{''}};
                {S,'att',{'contSet'},{''}};
                {type,'str',{'exact','approx'}}});

if isa(S,'double')
    res = in(E,S);
    return;
end

if ismethod(E,'distance')
    % use distance
    tmp = distance(E,S);
    res = tmp < E.TOL | withinTol(tmp,E.TOL);
else 
    % distance not implemented for S
    % use fallback
    % exact or over-approximative algorithm
   if strcmp(type,'exact')
       throw(CORAerror('CORA:noops',E,S));
   end

   % it is not certain that S implements quadMap
   if ismethod(S,'quadMap') && ismethod(E,'interval')
       res = isIntersectingMixed(E,S);
   else
       throw(CORAerror('CORA:noops',E,S));
   end
end

%------------- END OF CODE --------------