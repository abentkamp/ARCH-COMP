function val = norm(E,varargin)
% norm - compute the maximum Euclidean norm of an ellipsoid
%
% Syntax:  
%    val = norm(E,type)
%
% Inputs:
%    E    - ellipsoid object 
%    type - (optional) norm type (default: 2)
%
% Outputs:
%    val - value of the maximum norm
%
% Example: 
%    E = ellipsoid.generateRandom('Dimension',2);
%    val = norm(ellipsoid(E.Q,zeros(size(E.q))));
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      20-November-2019
% Last update:  31-July-2020
% Last revision:---

%------------- BEGIN CODE --------------

% default type
type = 2;

% parse input arguments
if nargin == 2 && ~isempty(varargin{1}) 
    if ~( isnumeric(varargin{1}) && ~any(varargin{1} == [1,2,Inf]) ) ...
            || ~( ischar(varargin{1}) && strcmp(varargin{1},'fro') )
        throw(CORAerror('CORA:wrongValue','second',"1, 2, Inf, or 'fro'"));
    end
    type = varargin{1};
end

if type ~= 2
    throw(CORAerror('CORA:noExactAlg',E,type,...
        'Only implemented for Euclidean norm'));
end

if ~all(E.q==0)
    throw(CORAerror('CORA:noExactAlg',E,type,...
        'Not yet implemented for non-zero center'));
end

% transform into eigenspace
lmax = max(eig(E.Q));
val = sqrt(lmax);

%------------- END OF CODE --------------