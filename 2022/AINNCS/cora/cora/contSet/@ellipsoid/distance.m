function val = distance(E,S)
% enclose - Computes the distance between an ellipsoid and the another set
%    representation or a point
%
% Syntax:  
%    val = distance(E,S)
%
% Inputs:
%    E - ellipsoid object
%    S - contSet object or cell array thereof
%
% Outputs:
%    val - distance(s) between ellipsoid and set/point
%
% Example:
%    E = ellipsoid(eye(2));
%    S = halfspace([1 1]/sqrt(2),-2);
%    distance(E,S)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      08-March-2021
% Last update:  18-March-2021 (allowing cell arrays)
% Last revision:---

%------------- BEGIN CODE --------------

%% parsing & checking
if ~isa(E,'ellipsoid')
    throw(CORAerror('CORA:wrongValue','first',"be of type 'ellipsoid'"));
end

% handle empty cells, S not a cell etc
[S,size_S] = prepareSetCellArray(S,E);
if isempty(S)
    % distance to empty set = 0 since empty-set \subseteq obj
    val = 0;
    return;
end
N = length(S);

%% different distances
val = zeros(N,1);
if isa(S{1},'ellipsoid')
    for i=1:N
        val(i) = distanceEllipsoid(E,S{i});
    end
    
elseif isa(S{1},'conHyperplane')
    % conHyperplane actually is a hyperplane
    for i=1:N
        if isHyperplane(S{i})
            val(i) = distanceHyperplane(E,S{i});
        else
            % use mptPolytope implementation
            S{i} = mptPolytope(S{i});
            val(i) = distanceMptPolytope(E,S{i});
        end
    end
    
elseif isa(S{1},'halfspace')
    % convert to mptPolytope
    for i=1:N
        val(i) = distanceMptPolytope(E,mptPolytope(S{i}));
    end
    
elseif isa(S{1},'mptPolytope')
    for i=1:N
        val(i) = distanceMptPolytope(E,S{i});
    end
    
elseif isa(S{1},'double')
    val = distancePoint(E,S);

else

    throw(CORAerror('CORA:noops',E,S{1}));
end

val = reshape(val,size_S);

%------------- END OF CODE --------------