function res = isFullDim(E)
% isFullDim - checks if the dimension of the affine hull of an ellipsoid is
%    equal to the dimension of its ambient space
%
% Syntax:  
%    res = isFullDim(E)
%
% Inputs:
%    E - ellipsoid object
%
% Outputs:
%    res - true/false
%
% Example:
%    E1 = ellipsoid(eye(2));
%    isFullDim(E1)
%
%    E2 = ellipsoid([1 0; 0 0]);
%    isFullDim(E2)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/isFullDim

% Author:       Niklas Kochdumper, Mark Wetzlinger, Victor Gassmann
% Written:      02-January-2020 
% Last update:  24-March-2022 (remove dependency on object property)
% Last revision:---

%------------- BEGIN CODE --------------

res = rank(E) == dim(E);

%------------- END OF CODE --------------