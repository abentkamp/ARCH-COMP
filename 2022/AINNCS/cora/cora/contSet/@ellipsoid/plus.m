function E = plus(E,S,varargin)
% plus - Overloaded '+' operator for approximating the Minkowski sum of an
%    ellipsoid and another set 
%
% Syntax:  
%    E = plus(E,S)
%    E = plus(E,S,mode)
%    E = plus(E,S,L)
%    E = plus(E,S,L,mode)
%
% Inputs:
%    E - ellipsoid object
%    S - set representation (or cell array thereof)
%    L - (optional) directions to use for approximation
%    mode - (optional) type of approximation ('i': inner; 'o': outer)
%           also: when L is empty, 'o:halder' is available
%
% Outputs:
%    E - ellipsoid object after Minkowski sum
%
% Example: 
%    ---
%
% References:
%   [1] Kurzhanskiy, A.A. and Varaiya, P., 2006, December. Ellipsoidal
%       toolbox (ET). In Proceedings of the 45th IEEE Conference on
%       Decision and Control (pp. 1498-1503). IEEE.
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      09-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

%% parsing & checking
% check if first or second argument is ellipsoid
if ~isa(E,'ellipsoid') 
    tmp = E;
    E = S;
    S = tmp;
end

% parse arguments
if isempty(varargin)
    L = zeros(length(E.q),0);
    mode = 'o';
elseif length(varargin)==1
    if isa(varargin{1},'char')
        mode = varargin{1};
        L = zeros(length(E.q),0);
    elseif isa(varargin{1},'double')
        L = varargin{1};
        mode = 'o';
    else
        throw(CORAerror('CORA:wrongValue','third',...
            "be of type 'double' or 'char'"));
    end
elseif length(varargin)==2
    if ~isa(varargin{1},'double')
         throw(CORAerror('CORA:wrongValue','third',"be of type 'double'"));
    end
    if ~isa(varargin{1},'char')
        throw(CORAerror('CORA:wrongValue','third',"be of type 'char'"));
    end
    L = varargin{1};
    mode = varargin{2};
else
    throw(CORAerror('CORA:tooManyInputArgs',4));
end

% handle empty cells, S not a cell etc
S = prepareSetCellArray(S,E);
if isempty(S)
    return;
end

if size(L,1)~=length(E.q)
    throw(CORAerror('CORA:dimensionMismatch','obj1',E,'dim1',dim(E),...
        'obj2',L,'size2',size(L)));
end

N = length(S);

%% different Minkowski additions
if isa(S{1},'double')
    s = sum(cell2mat(reshape(S,[1,numel(S)])),2);
    E = ellipsoid(E.Q,E.q+s);
    return;
end

if isa(S{1},'conPolyZono')
    E = S{1} + E; 
    for i=2:N
        E = S{i} + E; 
    end
    return; 
end

if isa(S{1},'ellipsoid')
   E = plusEllipsoid(E,S,L,mode);
   return;
end

% throw error for all other combinations
throw(CORAerror('CORA:noops',E,S{1}));

%------------- END OF CODE --------------