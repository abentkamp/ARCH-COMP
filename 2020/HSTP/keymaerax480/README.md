Repeatability instructions KeYmaera X 4.8.0:

1. Run `setup.sh`, this will initialize the Docker container and finalize the Wolfram Engine activation (https://www.wolfram.com/engine/); after this step, the license is saved on the host system in Licensing/mathpass. This step needs to be repeated when the license expires.
2. Run `run.sh`, this starts the container and runs the benchmarks; after this step, the results are in the results/ subfolder.