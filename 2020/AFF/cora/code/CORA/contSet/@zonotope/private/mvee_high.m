clear all;
n = 2;
dm = 10;
runs = 10;
steps = 3;
dV = zeros(steps,runs);
T = zeros(steps,runs);
E_cell = cell(steps,runs);
for s=1:steps
    for it=1:runs
        m = dm*s;
        G = randn(n,m);
        c = randn(n,1);
        Z = zonotope([c,G]);
        t = tic;
        E = MVEE(Z);
        %E = enc_ellipsoid(Z,'exact');
        E_cell{s,it} = E;
        T(s,it) = toc(t);
        dV(s,it) = volume(Z)/volume(E);
        display(['Step: ', num2str(s), '; Run: ', num2str(it)]);
    end
end
save('E_cell.mat','E_cell');