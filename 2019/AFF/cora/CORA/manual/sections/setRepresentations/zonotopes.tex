A zonotope is a geometric object in $\mathbb{R}^n$. Zonotopes are parameterized by a center $c \in \mathbb{R}^n$ and generators $g\^i \in \mathbb{R}^n$ and defined as
\begin{equation} \label{eq:zonotope}
	\mathcal{Z} = \Big\{ c + \sum_{i=1}^{p} \beta_i g\^{i} \Big| \beta_i \in [-1,1] \Big\}.
\end{equation}
We write in short $\mathcal{Z}=(c,g\^{1},\ldots,g\^{p})$. A zonotope can be interpreted as the Minkowski addition of line segments $l\^i = [-1,1]g\^{i}$ and is visualized step-by-step in a two-dimensional vector space in Fig. \ref{fig:zonotope}. Zonotopes are a compact way of representing sets in high dimensions. More importantly, operations required for reachability analysis, such as linear maps $M\otimes \mathcal{Z}:=\{M z |z \in \mathcal{Z}\}$ ($M\in\mathbb{R}^{q\times n}$) and Minkowski addition $\mathcal{Z}_1\oplus \mathcal{Z}_2 := \{z_1 + z_2 | z_1 \in \mathcal{Z}_1 , z_2 \in \mathcal{Z}_2\}$ can be computed efficiently and exactly, and others such as convex hull computation can be tightly over-approximated \cite{Girard2005}. 

\begin{figure}[htb]	

  \footnotesize
  \psfrag{#c}[][]{$c$}						
  \psfrag{#l1}[][]{$l\^1$}	
  \psfrag{#l2}[][]{$l\^2$}	
  \psfrag{#l3}[][]{$l\^3$}					
    \centering		
      \subfigure[$c\oplus l\^1$]
		{\includegraphics[width=0.2\columnwidth]{./figures/Z1bGray.eps}}
      \subfigure[$c\oplus l\^1 \oplus l\^2$]
		{\includegraphics[width=0.2\columnwidth]{./figures/Z2bGray.eps}}			
      \subfigure[$c\oplus \ldots \oplus l\^3$]
		{\includegraphics[width=0.2\columnwidth]{./figures/Z3bGray.eps}}					
      \caption{Step-by-step construction of a zonotope.}
      \label{fig:zonotope}
\end{figure}


We support the following methods for zonotopes:
\begin{itemize}
 \item \texttt{box} -- computes an enclosing axis-aligned box in generator representation.
 \item \texttt{cartesianProduct} -- returns the Cartesian product of two zonotopes.
 \item \texttt{center} -- returns the center of the zonotope.
 \item \texttt{constrSat} -- checks if all values of a zonotope satisfy the constraint $Cx<=d$, $C\in\mathbb{R}^{m\times n}$, $d\in\mathbb{R}^m$.
 \item \texttt{containsPoint} -- determines if a point is inside a zonotope.
 \item \texttt{conZonotope} -- converts a zonotope object into a constrained zonotope object.
 \item \texttt{deleteAligned} -- combines aligned generators to a single generator. This reduces the order of a zonotope while not causing any over-approximation.
 \item \texttt{deleteZeros} -- deletes generators whose entries are all zero.
 \item \texttt{dim} -- returns the dimension of a zonotope in the sense that the rank of the generator matrix is computed.
 \item \texttt{display} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}.
 \item \texttt{enclose} -- generates a zonotope that encloses two zonotopes of equal dimension according to \cite[Equation 2.2 + subsequent extension]{Althoff2010a}.
 \item \texttt{enclosingPolytope} -- converts a zonotope to a polytope representation in an over-approximative way to save computational time. The technique can be influenced by options, but most techniques are inspired by \cite[Sec.~2.5.6]{Althoff2010a}.
 \item \texttt{enlarge} -- enlarges the generators of a zonotope by a vector of factors for each dimension.
 \item \texttt{generators} -- returns the generators of a zonotope as a matrix whose column vectors are the generators.
 \item \texttt{in} -- determines if a zonotope is enclosed by another zonotope.
 \item \texttt{inParallelotope} -- checks if a zonotope is a subset of a parallelotope, where the latter is represented as a zonotope. 
 \item \texttt{interval} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in \cite[Proposition 2.2]{Althoff2010a}.
 \item \texttt{isempty} -- returns 1 if a zonotope is empty and 0 otherwise.
 \item \texttt{isIntersecting} -- determines if a set intersects a zonotope.
 \item \texttt{mtimes} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:zono_mtimes}.
 \item \texttt{mptPolytope} -- converts a zontope object into a mptPolytope object.
 \item \texttt{norm} -- computes the maximum norm value of all points in a zonotope.
 \item \texttt{plot} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}.
 \item \texttt{plotFilled} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}.
 \item \texttt{plus} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:zono_plus}.
 \item \texttt{polygon} -- converts a two-dimensional zonotope into a polygon and returns its vertices.
 \item \texttt{polytope} -- returns an exact polytope in halfspace representation according to \cite[Theorem 2.1]{Althoff2010a}. 
 \item \texttt{project} -- returns a zonotope, which is the projection of the input argument onto the specified dimensions.
 \item \texttt{quadraticMultiplication} -- given a zonotope $\mathcal{Z}$ and a discrete set of matrices $Q\^i\in\mathbb{R}^{n\times n}$ for $i=1\ldots n$, \texttt{quadraticMultiplication} computes $\{\varphi | \varphi_i=x^T Q\^i x, \, x\in\mathcal{Z}\}$ as described in \cite[Lemma 1]{Althoff2014a}.
 \item \texttt{quadZonotope} -- converts a zonotope to a \texttt{quadZonotope} object.
 \item \texttt{randPoint} -- generates a random point within a zonotope.
 \item \texttt{randPointExtreme} -- generates a random extreme point of a zonotope.
 \item \texttt{reduce} -- returns an over-approximating zonotope with fewer generators as detailed in Sec. \ref{sec:zono_reduce}.
 \item \texttt{split} -- splits a zonotope into two or more zonotopes that enclose the original zonotope. More details can be found in Sec. \ref{sec:zono_split}.
 \item \texttt{underapproximate} -- returns the vertices of an under-approximation. The under-approximation is computed by finding the vertices that are extreme in the direction of a set of vectors, stored in the matrix S. If S is not specified, it is constructed by the vectors spanning an over-approximative parallelotope.
 \item \texttt{vertices} -- returns a \texttt{vertices} object including all vertices of the zonotope
(Warning: high computational complexity). 
 \item \texttt{volume} -- computes the volume of a zonotope according to \cite[p.40]{Gover2010}.
 \item \texttt{zonotope} -- constructor of the class.
\end{itemize}


\subsubsection{Method \texttt{mtimes}} \label{sec:zono_mtimes}

Table \ref{tab:zono_mtimes} lists the classes that can be multiplied with a zonotope. Please note that the order plays a role and that the zonotope has to be on the right side of the \texttt{'*'} operator.

\begin{table}[h]
\caption{Classes that can be multiplied with a zonotope.}
\begin{center}\label{tab:zono_mtimes}
\begin{tabular}{lll}
	\hline 
	\textbf{class} & \textbf{reference} & \textbf{literature} \\ \hline
	MATLAB matrix & - & - \\
	\texttt{interval} & Sec. \ref{sec:interval} & \cite[Theorem 3.3]{Althoff2010a} \\
	\texttt{intervalMatrix} & Sec. \ref{sec:intervalMatrix} & \cite[Theorem 3.3]{Althoff2010a} \\
	\texttt{matZonotope} & Sec. \ref{sec:zonotopeMatrix} & \cite[Sec. 4.4.1]{Althoff2011b} \\ \hline
\end{tabular}
\end{center}
\end{table}


\subsubsection{Method \texttt{plus}} \label{sec:zono_plus}

Table \ref{tab:zono_plus} lists the classes that can be added to a zonotope. Unlike with multiplication, the zonotope can be on both sides of the \texttt{'+'} operator.

\begin{table}[h]
\caption{Classes that can be added to a zonotope.}
\begin{center}\label{tab:zono_plus}
\begin{tabular}{lll}
	\hline 
	\textbf{class} & \textbf{reference} & \textbf{literature} \\ \hline
	MATLAB vector & - & - \\
	\texttt{zonotope} & Sec. \ref{sec:zonotope} & \cite[Equation 2.1]{Althoff2010a} \\ \hline
\end{tabular}
\end{center}
\end{table}


\subsubsection{Method \texttt{reduce}} \label{sec:zono_reduce}

The zonotope reduction returns an over-approximating zonotope with fewer generators as described in \cite[Proposition 2.5]{Althoff2010a}. Table \ref{tab:zono_reduction} lists some of the implemented reduction techniques. The standard reduction technique is \texttt{'girard'}.

\begin{table}[h]
\caption{Reduction techniques for zonotopes.}
\begin{center}\label{tab:zono_reduction}
\begin{tabular}{lll}
	\hline 
	\textbf{technique} & \textbf{primary use} & \textbf{literature} \\ \hline
	\texttt{cluster} & Reduction to low order by clustering generators & \cite[Sec.~III.B]{Kopetzki2017} \\
	\texttt{combastel} & Reduction of high to medium order & \cite[Sec.~3.2]{Combastel2003} \\
	\texttt{constOpt} & Reduction to low order by optimization & \cite[Sec.~III.D]{Kopetzki2017} \\
	\texttt{girard} & Reduction of high to medium order & \cite[Sec. ~.4]{Girard2005} \\
	\texttt{methA} & Reduction to low order by volume minimization (A) & Meth. A, \cite[Sec.~2.5.5]{Althoff2010a} \\ 
	\texttt{methB} & Reduction to low order by volume minimization (B) & Meth. B, \cite[Sec.~2.5.5]{Althoff2010a} \\
	\texttt{methC} & Reduction to low order by volume minimization (C) & Meth. C, \cite[Sec.~2.5.5]{Althoff2010a} \\ 
	\texttt{pca} & Reduction of high to medium order using PCA & \cite[Sec.~III.A]{Kopetzki2017} \\ \hline
\end{tabular}
\end{center}
\end{table}

\subsubsection{Method \texttt{split}} \label{sec:zono_split}

The ultimate goal is to compute the reachable set of a single point in time or time interval with a single set representation. However, reachability analysis often requires abstractions of the original dynamics, which might become inaccurate for large reachable sets. In that event it can be useful to split the reachable set and continue with two or more set representations for the same point in time or time interval. Zonotopes are not closed under intersection, and thus not under splits. Several options as listed in Table \ref{tab:zono_split} can be selected to optimize the split performance.

\begin{table}[h]
\caption{Split techniques for zonotopes.}
\begin{center}\label{tab:zono_split}
\begin{tabular}{lll}
	\hline 
	\textbf{split technique} & \textbf{comment} & \textbf{literature} \\ \hline
	\texttt{splitOneGen} & splits one generator  & \cite[Proposition 3.8]{Althoff2010a} \\
	\texttt{directionSplit} & splits all generators in one direction & --- \\ 
	\texttt{directionSplitBundle} & exact split using zonotope bundles & \cite[Section V.A]{Althoff2011f} \\ 
	\texttt{halfspaceSplit} & split along a given halfspace & --- \\ \hline
\end{tabular}
\end{center}
\end{table}

\subsubsection{Zonotope Example} \label{sec:zonotopeExample}

The following MATLAB code demonstrates some of the introduced methods:

{\small
\input{./MATLABcode/example_zonotope.tex}}

This produces the workspace output
\begin{verbatim}
 Normalized, minimal representation polytope in R^2
         H: [8x2 double]
         K: [8x1 double]
    normal: 1
    minrep: 1
     xCheb: [2x1 double]
     RCheb: 1.4142

[ 0.70711     0.70711]      [  6.364]
[ 0.70711    -0.70711]      [ 2.1213]
[ 0.89443    -0.44721]      [ 3.3541]
[ 0.44721    -0.89443]      [ 2.0125]
[-0.70711    -0.70711] x <= [ 2.1213]
[-0.70711     0.70711]      [0.70711]
[-0.89443     0.44721]      [0.67082]
[-0.44721     0.89443]      [ 2.0125]

Intervals: 
[-1.5,5.5]
[-2.5,4.5]
\end{verbatim}

The plots generated in lines 9-12 are shown in Fig. \ref{fig:zonotopeExample_1} and the ones generated in lines 18-19 are shown in Fig. \ref{fig:zonotopeExample_2}.

\begin{figure}[h!tb]
\begin{minipage}{0.45\columnwidth}
  \centering
  %\footnotesize
  \psfrag{a}[c][c]{$x_1$}
  \psfrag{b}[c][c]{$x_2$}
    \includegraphics[width=\columnwidth]{./figures/setRepresentationTest/zonotopeTest_1.eps}
  \caption{Zonotopes generated in lines 9-12 of the zonotope example in Sec. \ref{sec:zonotopeExample}.}
  \label{fig:zonotopeExample_1}
\end{minipage}
\hspace{0.08\columnwidth}
\begin{minipage}{0.45\columnwidth}
  \centering
  %\footnotesize
  \psfrag{a}[c][c]{$x_1$}
  \psfrag{b}[c][c]{$x_2$}
    \includegraphics[width=\columnwidth]{./figures/setRepresentationTest/zonotopeTest_2.eps}
  \caption{Sets generated in lines 18-19 of the zonotope example in Sec. \ref{sec:zonotopeExample}.}
  \label{fig:zonotopeExample_2}
\end{minipage}
\end{figure}
