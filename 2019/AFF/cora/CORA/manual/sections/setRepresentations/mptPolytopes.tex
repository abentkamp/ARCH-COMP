There exist two representations for polytopes: The halfspace representation (H-representation) and the vertex representation (V-representation). The halfspace representation specifies a convex polytope $\mathcal{P}$ by the intersection of $q$ halfspaces $\mathcal{H}\^{i}$: $\mathcal{P}=\mathcal{H}\^{1} \cap \mathcal{H}\^{i} \cap \ldots \cap \mathcal{H}\^{q}$. A halfspace is one of the two parts obtained by bisecting the $n$-dimensional Euclidean space with a hyperplane $\mathcal{S}$, which is given by $\mathcal{S}:=\{x|c^T x= d\}, c \in\mathbb{R}^{n},d\in\mathbb{R}$. The vector $c$ is the normal vector of the hyperplane and $d$ the scalar product of any point on the hyperplane with the normal vector. From this follows that the corresponding halfspace is $\mathcal{H}:=\{x|c^T x\leq d\}$. As the convex polytope $\mathcal{P}$ is the nonempty intersection of $q$ halfspaces, $q$ inequalities have to be fulfilled simultaneously.

\paragraph{H-Representation of a Polytope}
A convex polytope $\mathcal{P}$ is the bounded intersection of $q$ halfspaces:
\[ \mathcal{P}=\Big\{x \in \mathbb{R}^n\big| C\, x\leq d \Big\}, \quad C\in\mathbb{R}^{q\times n}, d\in\mathbb{R}^{q}. \]
When the intersection is unbounded, one obtains a polyhedron \cite{Ziegler1995}.

A polytope with vertex representation is defined as the convex hull of a finite set of points in the $n$-dimensional Euclidean space. The points are also referred to as vertices and are denoted by $\mathtt{v}\^i\in\mathbb{R}^n$. A convex hull of a finite set of $r$ points $\mathtt{v}\^i\in\mathbb{R}^n$ is obtained from their linear combination:
\[ \mathtt{Conv}(\mathtt{v}\^1,\ldots,\mathtt{v}\^r):=\Big\{\sum_{i=1}^{r} \alpha_i \mathtt{v}\^i \big| \alpha_i \in\mathbb{R}, \, \alpha_i\geq 0, \, \sum_{i=1}^{r} \alpha_i=1 \Big\}. \]
Given the convex hull operator $\mathtt{Conv}()$, a convex and bounded polytope can be defined in vertex representation as follows:

\paragraph{V-Representation of a Polytope} For $r$ vertices $\mathtt{v}\^i\in\mathbb{R}^n$, a convex polytope $\mathcal{P}$ is the set $\mathcal{P}=\mathtt{Conv}(\mathtt{v}\^1,\ldots,\mathtt{v}\^r)$.

The halfspace and the vertex representation are illustrated in Fig. \ref{fig_polytope}. Algorithms that convert from H- to V-representation and vice versa are presented in \cite{Kaibel2003}. 

\begin{figure}[htb]		
	\psfrag{v}[r][c]{$\mathtt{v}\^{i}$}						
	\psfrag{p}[r][c]{$\mathtt{Conv}(\mathtt{v}\^1,\ldots,\mathtt{v}\^r)$}	
			
    \centering		
			\subfigure[$V-representation$]
				 {\includegraphics[height=2.5cm]{./figures/vertexRepresentation.eps}}
			\hspace{1cm}	
	\psfrag{b}[l][c]{$\mathcal{S}=\{x|c^T x= d\}$}	
	\psfrag{s}[l][c]{$\mathcal{H}\^i$}	
	\psfrag{p}[l][c]{$\mathcal{H}\^1\cap \mathcal{H}\^2\ldots\cap \mathcal{H}\^q$}
			\subfigure[$H-representation$]
				 {\includegraphics[height=2.5cm]{./figures/halfspaceRepresentation.eps}\label{fig:polytopeHRepresentation}}				
      \caption{Possible representations of a polytope.}
      \label{fig_polytope}
\end{figure}


The class \texttt{mptPolytope} is a wrapper class that interfaces with the MATLAB toolbox \textit{Multi-Parametric Toolbox} (MPT) for the following methods:
\begin{itemize}
 \item \texttt{and} -- computes the intersection of two \texttt{mptPolytopes}.
 \item \texttt{conZonotope} -- converts a \texttt{mptPolytope} object into a constrained zonotope.
 \item \texttt{dimension} -- returns the dimension of an \texttt{mptPolytope}.
 \item \texttt{display} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}.
 \item \texttt{enclose} -- computes the convex hull of two \texttt{mptPolytopes}.
 \item \texttt{in} -- determines if a \texttt{zonotope} is enclosed by a \texttt{mptPolytope}.
 \item \texttt{interval} -- encloses a \texttt{mptPolytope} by intervals of INTLAB.
 \item \texttt{interval} -- encloses a \texttt{mptPolytope} by an \texttt{interval}.
 \item \texttt{iscontained} -- returns if a \texttt{mptPolytope} is contained in another \texttt{mptPolytope}.
 \item \texttt{isempty} -- returns 1 if a \texttt{mptPolytope} is empty and 0 otherwise.
 \item \texttt{le} -- overloads the '<=' operator; returns 1 if one polytopes is equal or enclosed by the other one and 0 otherwise.
 \item \texttt{mldivide} -- computes the set difference of two \texttt{mptPolytopes}.
 \item \texttt{mldivide} -- computes the set difference $P_1\setminus P_2$ such that $P_2$ is subtracted from $P_1$.
 \item \texttt{mptPolytope} -- constructor of the class.
 \item \texttt{minus} -- overloaded '-' operator for the subtraction of a vector from an \texttt{mptPolytope} or the Minkowski difference between two \texttt{mptPolytope} objects.
 \item \texttt{mtimes} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations} for numeric and interval matrix multiplication. 
 \item \texttt{or} -- overloaded '|' operator to compute the union of two \texttt{mptPolytopes}.
 \item \texttt{plot} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}. 
 \item \texttt{plotFilled} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}.
 \item \texttt{project} -- projects a \texttt{mptPolytope} onto a set of dimensions.
 \item \texttt{plus} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations} for numeric vectors and \texttt{mptPolytope} objects.
 \item \texttt{vertices} -- returns a \texttt{vertices} object including all vertices of the polytope. 
 \item \texttt{volume} -- computes the volume of a polytope.
\end{itemize}



\subsubsection{MPT Polytope Example} \label{sec:mptPolytopeExample}

The following MATLAB code demonstrates some of the introduced methods:

{\small
\input{./MATLABcode/example_mptPolytope.tex}}

This produces the workspace output
\begin{verbatim}
Normalized, minimal representation polytope in R^2
         H: [8x2 double]
         K: [8x1 double]
    normal: 1
    minrep: 1
     xCheb: [2x1 double]
     RCheb: 2.8284

[ 0.70711    -0.70711]      [1.4142]
[       0          -1]      [     1]
[-0.70711    -0.70711]      [1.4142]
[      -1           0]      [     3]
[-0.70711     0.70711] x <= [4.2426]
[       0           1]      [     5]
[ 0.70711     0.70711]      [4.2426]
[       1           0]      [     3]

V: 
         0   -1.0000         0
         0    1.0000    2.0000
\end{verbatim}

The plot generated in lines 13-16 is shown in Fig. \ref{fig:mptPolytopeExample}.

\begin{figure}[h!tb]
  \centering
  %\footnotesize
  \psfrag{a}[c][c]{$x_1$}
  \psfrag{b}[c][c]{$x_2$}
    \includegraphics[width=0.8\columnwidth]{./figures/setRepresentationTest/mptPolytopeTest_1.eps}
  \caption{Sets generated in lines 13-16 of the MPT polytope example in Sec. \ref{sec:mptPolytopeExample}.}
  \label{fig:mptPolytopeExample}
\end{figure}