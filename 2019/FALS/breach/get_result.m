function get_result(pbs)
nb_trials = size(pbs,2);

for i =  1:size(pbs,1)
    name_phi{i} = pbs(i,1).Spec.req_monitors{1}.name;
    nb_falsified(i) = 0;
    falsif_at = [];
    
    for j = 1:nb_trials
        fval = pbs(i,j).obj_log;
        if any(fval<0)
            nb_falsified(i) = nb_falsified(i)+1;
            falsif_at(i,j) = find(fval<0, 1);
        end
        
    end
    
    if nb_falsified(i)>0
       mean_num_eval(i) = mean(falsif_at(falsif_at>0));
       median_num_eval(i)= median(falsif_at(falsif_at>0));
       fprintf('%s &&&& %g/%g & %g & %g \n', name_phi{i}, nb_falsified(i), nb_trials, mean_num_eval(i), median_num_eval(i));
    else
       fprintf('%s &&&& 0/%g & -- & -- \n', name_phi{i},  nb_trials);
    end
    
    
    
end
