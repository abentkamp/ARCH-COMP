altg   = 4040;
Vtg    = 540;
phig   = (pi/2)*0.5;
thetag = -(pi/2)*0.8;
psig   = -(pi/4);

addpath(genpath('../shared/benchmarks/F16_GCAS/AeroBenchVV-master'));
warning('off', 'f16:no_analysis');
